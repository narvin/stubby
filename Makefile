PYPATH := $(dir $(abspath $(lastword $(MAKEFILE_LIST))))
FORMAT := PYTHONPATH=$(PYPATH) black --exclude '{{'
LINT := PYTHONPATH=$(PYPATH) pylint --exit-zero -s n --ignore-paths '.*{{.*'
STYLE := PYTHONPATH=$(PYPATH) pycodestyle --exclude '{{'
TYPE := PYTHONPATH=$(PYPATH) mypy --no-error-summary --exclude '{{'
TEST := PYTHONPATH=$(PYPATH) pytest

.PHONY: all format lint style type

all: lint style type

format:
	@printf '===== FORMAT =====\n'
	@$(FORMAT) src tests

lint:
	@printf '===== LINT =====\n'
	@$(LINT) src tests

style:
	@printf '===== STYLE =====\n'
	@$(STYLE) src tests || true

type:
	@printf '===== TYPE =====\n'
	@$(TYPE) src tests || true

test:
	@printf '===== TEST =====\n'
	@$(TEST) tests
