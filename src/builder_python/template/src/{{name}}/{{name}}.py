"""A module in the main package."""


def main() -> int:
    """The main function."""
    return 0


if __name__ == "__main__":
    raise SystemExit(main())
