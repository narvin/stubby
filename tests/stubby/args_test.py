"""Tests for the args module."""

import os
import os.path

import pytest

from stubby.args import Args, PromptSpec, parse_args, prompt_args


class TestArgs:  # pylint: disable=too-few-public-methods
    """Test suite for the Args class."""

    @staticmethod
    def test_is_type_dict() -> None:
        """Args is a type(dict)."""
        assert isinstance(Args, type(dict))


class TestParseArgs:
    """Test suite for parse_args function."""

    @staticmethod
    @pytest.mark.parametrize(
        "args",
        [
            ["stubby"],
            ["stubby", "-s"],
            ["stubby", "-s", "-n", "foo"],
            ["stubby", "-s", "-o", "foo", "bar"],
            ["stubby", "-s", "-o", "foo", "bar", "-o", "baz", "quo"],
        ],
    )
    def test_builder_defaults_to_default(
        args: list[str], monkeypatch: pytest.MonkeyPatch
    ) -> None:
        """builder defaults to default."""
        monkeypatch.setattr("sys.argv", args)
        assert parse_args()["builder"] == "default"

    @staticmethod
    @pytest.mark.parametrize(
        "args, target",
        [
            (["stubby", "foo"], os.path.abspath("foo")),
            (["stubby", "-s", "foo"], os.path.abspath("foo")),
            (["stubby", "-s", "-n", "foo", "foo"], os.path.abspath("foo")),
            (
                ["stubby", "-s", "-o", "foo", "bar", "foo"],
                os.path.abspath("foo"),
            ),
            (
                ["stubby", "-s", "-o", "foo", "bar", "-o", "baz", "quo", "foo"],
                os.path.abspath("foo"),
            ),
            (["stubby"], os.getcwd()),
            (["stubby", "-s"], os.getcwd()),
            (["stubby", "-s", "-n", "foo"], os.getcwd()),
            (["stubby", "-s", "-o", "foo", "bar"], os.getcwd()),
            (
                ["stubby", "-s", "-o", "foo", "bar", "-o", "baz", "quo"],
                os.getcwd(),
            ),
        ],
    )
    def test_target_defaults_to_cwd(
        args: list[str], target: str, monkeypatch: pytest.MonkeyPatch
    ) -> None:
        """target, if not specified, is the current directory."""
        monkeypatch.setattr("sys.argv", args)
        parsed_args = parse_args()
        assert parsed_args["target"] == target

    @staticmethod
    @pytest.mark.parametrize(
        "args, silent",
        [
            (["stubby", "-n", "foo"], False),
            (["stubby", "--name", "foo"], False),
            (["stubby", "-s", "-n", "foo"], True),
            (["stubby", "--silent", "--name", "foo"], True),
        ],
    )
    def test_silent_defaults_to_false(
        args: list[str], silent: bool, monkeypatch: pytest.MonkeyPatch
    ) -> None:
        """silent defaults to True."""
        monkeypatch.setattr("sys.argv", args)
        parsed_args = parse_args()
        assert parsed_args["silent"] == silent

    @staticmethod
    @pytest.mark.parametrize(
        "args, name",
        [
            (["stubby", "-n", "foo"], "foo"),
            (["stubby", "--name", "foo"], "foo"),
            (["stubby", "-s", "-n", "foo"], "foo"),
            (["stubby", "-s", "--name", "foo"], "foo"),
            (["stubby"], os.path.basename(os.getcwd())),
            (["stubby", "-s"], os.path.basename(os.getcwd())),
            (["stubby", "-o", "foo", "bar"], os.path.basename(os.getcwd())),
        ],
    )
    def test_name_derived_from_target(
        args: list[str], name: str, monkeypatch: pytest.MonkeyPatch
    ) -> None:
        """name, if not specified, is the basename of the target directory."""
        monkeypatch.setattr("sys.argv", args)
        parsed_args = parse_args()
        assert parsed_args["name"] == name

    @staticmethod
    @pytest.mark.parametrize(
        "args",
        [
            ["stubby", "-o"],
            ["stubby", "--builder-opt"],
            ["stubby", "-o", "foo"],
            ["stubby", "--builder-opt", "foo"],
            ["stubby", "-o", "-s"],
            ["stubby", "--builder-opt", "-s"],
            ["stubby", "-o", "foo", "-s"],
            ["stubby", "--builder-opt", "foo", "-s"],
            ["stubby", "-s", "-o"],
            ["stubby", "-s", "--builder-opt"],
            ["stubby", "-s", "-o", "foo"],
            ["stubby", "-s", "--builder-opt", "foo"],
        ],
    )
    def test_builder_opt_takes_2_args(
        args: list[str],
        monkeypatch: pytest.MonkeyPatch,
        capsys: pytest.CaptureFixture[str],
    ) -> None:
        """builder_opt takes 2 args."""
        monkeypatch.setattr("sys.argv", args)
        with pytest.raises(SystemExit):
            parse_args()
        _, err = capsys.readouterr()
        assert "error: argument -o/--builder-opt: expected 2 arguments" in err

    @staticmethod
    @pytest.mark.parametrize(
        "args, builder_opts",
        [
            (["stubby", "-s", "-n", "foo"], {}),
            (
                ["stubby", "-s", "--builder-opt", "foo", "bar"],
                {"foo": "bar"},
            ),
            (
                ["stubby", "-s", "-o", "foo", "bar", "-o", "baz", "quo"],
                {"foo": "bar", "baz": "quo"},
            ),
        ],
    )
    def test_builder_opts_dict(
        args: list[str], builder_opts: dict[str, str], monkeypatch: pytest.MonkeyPatch
    ) -> None:
        """builder_opts is a dict[str, str]."""
        monkeypatch.setattr("sys.argv", args)
        parsed_args = parse_args()
        assert parsed_args["builder_opts"] == builder_opts

    @staticmethod
    @pytest.mark.parametrize(
        "args",
        [
            ["stubby", "-t"],
            ["stubby", "--template-var"],
            ["stubby", "-t", "foo"],
            ["stubby", "--template-var", "foo"],
            ["stubby", "-t", "-s"],
            ["stubby", "--template-var", "-s"],
            ["stubby", "-t", "foo", "-s"],
            ["stubby", "--template-var", "foo", "-s"],
            ["stubby", "-s", "-t"],
            ["stubby", "-s", "--template-var"],
            ["stubby", "-s", "-t", "foo"],
            ["stubby", "-s", "--template-var", "foo"],
        ],
    )
    def test_template_var_takes_2_args(
        args: list[str],
        monkeypatch: pytest.MonkeyPatch,
        capsys: pytest.CaptureFixture[str],
    ) -> None:
        """template_var takes 2 args."""
        monkeypatch.setattr("sys.argv", args)
        with pytest.raises(SystemExit):
            parse_args()
        _, err = capsys.readouterr()
        assert "error: argument -t/--template-var: expected 2 arguments" in err

    @staticmethod
    @pytest.mark.parametrize(
        "args, template_vars",
        [
            (["stubby", "-s", "-n", "foo", "default"], {}),
            (
                ["stubby", "-s", "--template-var", "foo", "bar", "default"],
                {"foo": "bar"},
            ),
            (
                ["stubby", "-s", "-t", "foo", "bar", "-t", "baz", "quo", "default"],
                {"foo": "bar", "baz": "quo"},
            ),
        ],
    )
    def test_template_vars_dict(
        args: list[str], template_vars: dict[str, str], monkeypatch: pytest.MonkeyPatch
    ) -> None:
        """template_vars is a dict[str, str]."""
        monkeypatch.setattr("sys.argv", args)
        parsed_args = parse_args()
        assert parsed_args["template_vars"] == template_vars


class TestPromptArgs:
    """Test suite for prompt_args function."""

    @staticmethod
    @pytest.mark.parametrize(
        "spec, partial_prompts",
        [
            ([(None, "foo", None)], ["foo"]),
            ([("Enter foo", "foo", None)], ["Enter foo"]),
            ([(None, "foo", None), ("Enter foo", "foo", None)], ["foo", "Enter foo"]),
        ],
    )
    def test_prompt_defaults_to_name(
        spec: PromptSpec, partial_prompts: list[str], monkeypatch: pytest.MonkeyPatch
    ) -> None:
        """The prompt, if not specified, defaults to the spec name."""
        prompts = []

        def mock_input(prompt: str) -> str:
            prompts.append(prompt)
            return ""

        monkeypatch.setattr("builtins.input", mock_input)
        prompt_args(spec)
        assert prompts == [f"{prompt}: " for prompt in partial_prompts]

    @staticmethod
    @pytest.mark.parametrize(
        "spec, inputs, res",
        [
            ([(None, "foo", None)], [""], {}),
            ([(None, "foo", "bar")], [""], {"foo": "bar"}),
            ([(None, "foo", None)], ["bar"], {"foo": "bar"}),
            ([("Enter foo", "foo", None)], [""], {}),
            ([("Enter foo", "foo", "bar")], [""], {"foo": "bar"}),
            ([("Enter foo", "foo", None)], ["bar"], {"foo": "bar"}),
            (
                [(None, "foo", None), ("Enter bar", "bar", "1"), (None, "baz", None)],
                ["2", "", ""],
                {"foo": "2", "bar": "1"},
            ),
        ],
    )
    def test_value_defaults_to_default(
        spec: PromptSpec,
        inputs: list[str],
        res: dict[str, str],
        monkeypatch: pytest.MonkeyPatch,
    ) -> None:
        """
        The value, if not specified, defaults to the spec default, and is omitted
        if it is ultimately empty.
        """

        def mock_input(_: str) -> str:
            return inputs.pop(0)

        monkeypatch.setattr("builtins.input", mock_input)
        assert prompt_args(spec) == res
